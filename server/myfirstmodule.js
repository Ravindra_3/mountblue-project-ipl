var http = require('http');
var fs = require('fs');
// first created the server on localhost using http
var server = http.createServer(function (req, res) {
        if (req.url === '/') {
            fs.readFile('./index.html', "utf-8", (err, data) => {
                if (err) {
                    console.error('file-does-not-exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this condition is used to get matches per year won
        } else if (req.url === '/matchesPerYear') {

            fs.readFile('../output/matchesPerYear.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get best super over economy of the boweler
        } else if (req.url === '/bestsuperover') {
            fs.readFile('../output/bestsuperovereconomy.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get top ten economical bwoler of all time in ipl
        } else if (req.url === '/besteconomical') {
            fs.readFile('../output/besteconomical2.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this condition is used to get most toss winer and match winner taem
        } else if (req.url === '/mosttossmostmatchwin') {
            fs.readFile('../output/mostmacthwin.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get the result of player of the match  on server per year in ipl
        } else if (req.url === '/playerOfthematch') {
            fs.readFile('../output/playerofthematch.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'json'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this function is used to claculate virat kholi strike rate per year
        } else if (req.url === '/viratkohliStrikerate') {
            fs.readFile('../output/viratstrikerate.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used for one bwoler dismismisd other batsman max time
        } else if (req.url === '/bowelersdissmisile') {
            fs.readFile('../output/bowelersdissmisile.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get match win per team in per year
        }else if (req.url === '/player-Of-thematch') {
            fs.readFile('./playerofthematch.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
        } 
        else if (req.url === '/bowelers-dissmisile') {
            fs.readFile('./mostdismisile.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
        } 
         else if (req.url === '/matchwin') {
            fs.readFile('../output/win.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this fucntion is used to get extra run concedded by teams in per season
        } else if (req.url === '/Match-win') {
            fs.readFile('./win.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
        }    else if (req.url === '/extrarun') {
            fs.readFile('../output/extrarun.json', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get the statics of extra run
        } else if (req.url === '/extra-run') {
            fs.readFile('./extrarun.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get statics of most toss winer and match winer
        } else if (req.url === '/most-toss-most-matchwin') {
            fs.readFile('./mosttosswiner.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get best super over econmical bwoler
        } else if (req.url === '/best-superover') {
            fs.readFile('./bestsup.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get ststics of best econominical bwoler
        } else if (req.url === '/best-economical') {
            fs.readFile('./besteconmybowler.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is used to get statics of matches per year won
        } else if (req.url === '/matches-Per-Year') {
            fs.readFile('./matchperyearwon.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this function is used to get strike rate of virat khli per season
        } else if (req.url === '/virat-kholi-strike') {
            fs.readFile('./viratkohli.html', 'utf-8', (err, data) => {
                if (err) {
                    console.error('file  does not exist')
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(data);
                    res.end();
                }
            })
            // this is the by defult page which is used to get if error or somthing is not good
        } else {
            res.writeHead(404, {
                'Content-Type': 'text/html'
            });
            res.write('route does not exist');
            res.end();
        }

    })
    .listen(8080, () => {
        console.log("server_started")
    })