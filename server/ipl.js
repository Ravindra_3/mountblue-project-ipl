module.exports = class Matchplayed {
    constructor(jsonObj) {
        this.matchplay = jsonObj;
    }
    //   this function is for use of count  the number of matches played per year
    Matchplayed1() {
        var arr1 = []
        for (let i = 0; i < this.matchplay.length; i++) {
            arr1.push(this.matchplay[i].season)
        }
        var counts = {}
        for (let i = 0; i < arr1.length; i++) {
            var nums = arr1[i]
            counts[nums] = counts[nums] ? counts[nums] + 1 : 1
        }

        return counts

    }
    // this function is used for count the number of win matches per team in per year
    win() {

        var winer = {}
         for(let i=0;i<this.matchplay.length;i++){

            
            
    
                let team = this.matchplay[i]
                if (winer.hasOwnProperty(team.season)) {
    
                    if (winer[team.season].hasOwnProperty(team.winner)) {
                        winer[team.season][team.winner] += 1
    
                    } else {
                        winer[team.season][team.winner] = 1
                    }
                } else {
                    winer[team.season] = {}
                    winer[team.season][team.winner] = 1
                }
            
               


            }
        
      
            return winer



}
    // this function is used for extra run conceded by per team in each season
    extrarun(arr1) {

        var teams = {}

        for (let i = 0; i < this.matchplay.length; i++) {

            let temp = this.matchplay[i]
            if (arr1.indexOf(temp.match_id) != -1) {
                if (teams.hasOwnProperty(temp.bowling_team)) {

                    teams[temp.bowling_team] += parseInt(temp.extra_runs)

                } else {
                    teams[temp.bowling_team] = parseInt(temp.extra_runs)
                }

            }


        }
        return teams


    }
    // this function is added for filter the match id according to years
    year(x) {
        let arr=[]

        var k=this.matchplay.filter((curr)=>{
            if(curr.season==x){
            arr.push(curr.id) }
             })

       
        return arr
    }
    //this is to get top ten besteconamical bowler in ipl 
    econmical(arr2) {

        var bowlers = {}
        for (let i = 0; i < this.matchplay.length; i++) {
            let temp = this.matchplay[i]

            if (arr2.indexOf(temp.match_id) != -1) {
                if (bowlers.hasOwnProperty(temp.bowler)) {
                    bowlers[temp.bowler]["totalruns"] += parseInt(temp.batsman_runs) + parseInt(temp.noball_runs) + parseInt(temp.wide_runs)
                    if (temp.noball_runs == "0" && temp.wide_runs == "0") {
                        bowlers[temp.bowler]["totalbals"] += 1
                    }
                } else {
                    bowlers[temp.bowler] = {};
                    bowlers[temp.bowler]['totalruns'] = parseInt(temp.batsman_runs) + parseInt(temp.noball_runs) + parseInt(temp.wide_runs)
                    if (temp.noball_runs == "0" && temp.wide_runs == "0") {
                        bowlers[temp.bowler]["totalbals"] = 1
                    } else {
                        bowlers[temp.bowler]["totalbals"] = 0
                    }
                }

            }


        }

        // var result = Object.keys(bowlers)
        var final_result = {}
        Object.keys(bowlers).forEach((curr)=>{
        final_result[curr] = (bowlers[curr]["totalruns"] / (bowlers[curr]["totalbals"])) * 6
       })

        var keysSorted = Object.keys(final_result).sort(function (a, b) {
            return final_result[a] - final_result[b]
        })

        var econmicalbwoler = {}


        for (let i = 0; i < 10; i++) {
            econmicalbwoler[keysSorted[i]] = final_result[keysSorted[i]]

        }
        return econmicalbwoler
    }
    //this is used to get  maximum player of the match win in per season
    playerofthematch() {
        var count = {}
        for (let i = 0; i < this.matchplay.length; i++) {
            var temp = this.matchplay[i]

            if (count.hasOwnProperty(temp.season)) {
                if (count[temp.season].hasOwnProperty(temp.player_of_match)) {
                    count[temp.season][temp.player_of_match] += 1
                } else {
                    count[temp.season][temp.player_of_match] = 1
                }

            } else {
                count[temp.season] = {}
                count[temp.season][temp.player_of_match] = 1
            }

        }
        var final_count = {}
        var arr2 = Object.keys(count)
        for (let i = 0; i < arr2.length; i++) {

            var arr9 = Object.keys(count[arr2[i]]).sort(function (a, b) {
                return count[arr2[i]][a] - count[arr2[i]][b]
            })
            let v = arr9[arr9.length - 1]
            final_count[arr2[i]] = {}

            final_count[arr2[i]][v] = count[arr2[i]][v]

        }

        return final_count
    }
    // this is used to get macth id for each season
    object1() {
        var count = {}
        for (let i = 0; i < this.matchplay.length; i++) {
            count[this.matchplay[i].id] = this.matchplay[i].season
        }
        return count

    }
    // this is used to get viratkholi strike rate per season
    viratstrike1(count) {
        var count1 = {}
        for (let i = 0; i < this.matchplay.length; i++) {
            if (this.matchplay[i].batsman == 'V Kohli') {
                let y = this.matchplay[i].match_id
                let z = count[y]
                if (count1.hasOwnProperty(z)) {
                    count1[z]["totalruns"] += parseInt(this.matchplay[i].batsman_runs)
                    if (this.matchplay[i].wide_runs == "0") {

                        count1[z]["totalballs"] += 1
                    }

                } else {

                    count1[z] = {}

                    count1[z]["totalruns"] = parseInt(this.matchplay[i].batsman_runs)
                    if (this.matchplay[i].wide_runs === "0" && this.matchplay[i].noball_runs == "0") {

                        count1[z]["totalballs"] = 1
                    } else {
                        count1[z]["totalballs"] = 0
                    }
                }}}
        var arry = Object.keys(count1)
        var count3 = {}
        for (let i = 0; i < arry.length; i++) {
            let d = arry[i]
            count3[d] = (count1[d].totalruns) / (count1[d].totalballs) * 100
        }

        return count3

    }
    // this is used to get number of dissmisle by oter player out
    bowelerdissmisile() {
        var dismissedplayer = {}, arrayofObj = []
        this.matchplay.map(function (data) {
          if (data.player_dismissed != '') {
            if (dismissedplayer.hasOwnProperty(data.player_dismissed)) {
              if (dismissedplayer[data.player_dismissed].hasOwnProperty(data.bowler)) {
                dismissedplayer[data.player_dismissed][data.bowler] += 1
              } else {
                dismissedplayer[data.player_dismissed][data.bowler] = 1
              }
            } else {
              dismissedplayer[data.player_dismissed] = {}
              dismissedplayer[data.player_dismissed][data.bowler] = 1
            }
          }
        })
        var posi = 0, posj = 0
        var max = 0, value = 0, resobj1 = {}
        for (let i in dismissedplayer) {
          for (let j in dismissedplayer[i]) {
            value = dismissedplayer[i][j]
            if (max < value) {
              max = dismissedplayer[i][j]
              posi = i
              posj = j
            }
          }
        }
        return { 'batsman': posi, 'bowler': posj, 'num': max }
      }
    
    // it is used to get superover economy of bowler
    superovereconmy() {

        var count = {}
        for (let i = 0; i, i < this.matchplay.length; i++) {
            let temp = this.matchplay[i]
            let temp1 = this.matchplay[i]["is_super_over"]

            if (temp1 != 0) {
                if (count.hasOwnProperty(temp.bowler)) {
                    count[temp.bowler]["totalruns"] += parseInt(temp.batsman_runs) + parseInt(temp.noball_runs) + parseInt(temp.wide_runs)
                    if (temp.noball_runs == "0" && temp.wide_runs == "0") {
                        count[temp.bowler]["totalbals"] += 1
                    }

                } else {
                    count[temp.bowler] = {};
                    count[temp.bowler]['totalruns'] = parseInt(temp.batsman_runs) + parseInt(temp.noball_runs) + parseInt(temp.wide_runs)
                    if (temp.noball_runs == "0" && temp.wide_runs == "0") {
                        count[temp.bowler]["totalbals"] = 1
                    } else {
                        count[temp.bowler]["totalbals"] = 0
                    }
                }


            }


        }
        var result = Object.keys(count)
        var final_result = {}
        for (let i = 0; i < result.length; i++) {
            let temp = result[i]
            final_result[temp] = (count[temp]["totalruns"] / (count[temp]["totalbals"])) * 6
        }
        var keysSorted = Object.keys(final_result).sort(function (a, b) {
            return final_result[a] - final_result[b]
        })
        var boweler = {}
        boweler[keysSorted[0]] = final_result[keysSorted[0]]
        return boweler
    }
    // this function is used for  most matches win who win most tosses
    mostmatchmosttoss() {
        var count = {}

        this.matchplay.forEach(function (key) {
            var d = key.toss_winner
            var e = key.winner
            if (d == e) {
                if (count.hasOwnProperty(d)) {
                    count[d] += 1
                } else
                    count[d] = 1
            }
            return count
        })


             return count
    }

};
